/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.alodha3.service;

import edu.iit.sat.itmd4515.alodha3.domain.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.*;
import javax.sql.DataSource;
import javax.annotation.Resource;
import javax.ejb.Stateless;

/**
 *
 * @author anku8
 */
@Stateless
public class CustomerService {
        
    private static final Logger LOG = Logger.getLogger(CustomerService.class.getName());
    
    @Resource(lookup = "jdbc/itmd4515DS")
    DataSource ds;

    /**
     *
     */
    public CustomerService() {
    }
    
    /**
     *
     * @param id
     * @return
     */
    public Customer findCustomer(Long id){
        Customer customer = null;
        String sql = "select * from Customer where CustomerId = ?";
       
        try( Connection c = ds.getConnection()){

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setLong(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                customer = new Customer(rs.getLong("CustomerId"),
                                 rs.getString("FirstName"),
                                 rs.getString("LastName"));
                
                LOG.info(customer.toString()); 
            }
        
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return customer;
    }
}
