/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.alodha3.service;

import edu.iit.sat.itmd4515.alodha3.domain.Employee;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author anku8
 */
@Stateless
public class EmployeeService {
    
    @PersistenceContext(unitName = "itmd4515PU")
    private EntityManager em;

    /**
     *
     */
    public EmployeeService() {
    }
    
    /**
     *
     * @param id
     * @return
     */
    public Employee findEmployeeById(Long id){
        return em.find(Employee.class,id);
    
    }
    
    /**
     *
     * @return
     */
    public List<Employee> findEmployees(){
        return em.createQuery("select e from Employee e", Employee.class).getResultList();
    }
}
